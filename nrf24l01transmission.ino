#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#define Xpin A2
#define Ypin A1

RF24 radio(9,10);
const byte address[6] = "00001";
int Array[2];
int test = 10;

struct Data_Transmission {
   int Xval;
  int Yval;
  };

Data_Transmission data;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  radio.begin();
  radio.openWritingPipe(address);
  radio.setAutoAck(false);
  radio.setDataRate(RF24_250KBPS);
  radio.stopListening();
  radio.setPALevel(RF24_PA_LOW);
  radio.setChannel(4);

  data.Xval = 0;
  data.Yval = 0;
}

void loop() {
  // put your main code here, to run repeatedly:
  Array[0] = map(analogRead(Xpin),0, 1023, -255, 255);
  Array[1] = map(analogRead(Ypin),0, 1023, -255, 255);
  radio.write(&Array, sizeof(Array));
  Serial.println("Sending");
}